mod fractal;
mod fun;
mod help;
mod randomword;
mod totp;

use crate::hosts::Hosts;
use actix_web::{web, HttpResponse, Result, Route};

async fn favicon() -> Result<HttpResponse> {
    Ok(HttpResponse::Ok()
        .content_type("image/png")
        .body(crate::statics::images::FUZEN_INFO_FAVICON))
}

fn info() -> Route {
    web::get().guard(Hosts::FuzenInfo)
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum FuzenInfoRoutes {
    Help,
    Totp,
    RandomWord,
    Fractal,
    Hello,
    Baka,
    Favicon,
}

impl FuzenInfoRoutes {
    pub fn cors() -> actix_cors::Cors {
        actix_cors::Cors::default()
    }
}

impl From<FuzenInfoRoutes> for Route {
    fn from(val: FuzenInfoRoutes) -> Self {
        use FuzenInfoRoutes::*;
        match val {
            Help => info().to(help::help),
            Hello => info().to(fun::Hello::route),
            Totp => info().to(totp::TOTP::route),
            RandomWord => info().to(randomword::randomword),
            Fractal => info().to(fractal::fractal_png),
            Baka => info().to(fun::Baka::route),
            Favicon => info().to(favicon),
        }
    }
}
